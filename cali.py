#!/usr/bin/python
import web
import smbus
import math
import csv
urls = (
    '/', 'index'
)

# Power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
address = 0x68       # This is the address value read via the i2cdetect command


def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def dist(a,b):
    return math.sqrt((a*a)+(b*b))

def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)

def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)


class index:
    def GET(self):
        from read import fetch_values
        gyro_inst, accel_inst = fetch_values()
        x_rotation = get_x_rotation(accel_inst[0],
                                    accel_inst[1],
                                    accel_inst[2])

        y_rotation = get_y_rotation(accel_inst[0],
                                    accel_inst[1],
                                    accel_inst[2])

        with open('data.csv') as f:
            reader = csv.reader(f)

            #Fetch first 3 lines
            gyro_avg = next(reader)
            accel_avg = next(reader)
            rotation = next(reader)

            gyro_avg = ''.join(gyro_avg).split() 
            accel_avg = ''.join(accel_avg).split()
            rotation = ''.join(rotation).split()

            cx_rotation, cy_rotation = rotation[0], rotation[1]

        return str(x_rotation-float(cx_rotation))+" "+str(y_rotation-float(cy_rotation))


if __name__ == "__main__":

    # Now wake the 6050 up as it starts in sleep mode
    bus.write_byte_data(address, power_mgmt_1, 0)

    app = web.application(urls, globals())
    app.run()


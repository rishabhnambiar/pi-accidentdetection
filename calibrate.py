import smbus, math, time, operator, csv
from read import *

initialize()


def calibrate(seconds):
    from read import fetch_values
    gyro_sum, accel_sum = (0.0,0.0,0.0), (0.0,0.0,0.0)

    for second in range(seconds):
        seconds = (seconds, seconds, seconds)
        gyro, accel = fetch_values()
        gyro_sum = tuple(map(operator.add, gyro_sum, gyro))
        accel_sum = tuple(map(operator.add, accel_sum, accel))
        time.sleep(1)
        print gyro, accel

    gyro_avg = tuple(map(operator.div, gyro_sum, (10,10,10)))
    accel_avg = tuple(map(operator.div, accel_sum, (10,10,10)))

    x_rotation = get_x_rotation(accel_avg[0],
                                accel_avg[1],
                                accel_avg[2])

    y_rotation = get_y_rotation(accel_avg[0],
                                accel_avg[1],
                                accel_avg[2])
    
    rotation = (x_rotation, y_rotation)

    write_values(gyro_avg, accel_avg, rotation)

    print gyro_avg, accel_avg, rotation
    return gyro_avg, accel_avg, rotation
    
def write_values(g, a, r):
    with open("data.csv", "w") as file:
        csv.register_dialect("custom", delimiter=" ", skipinitialspace=True)
        writer = csv.writer(file, dialect="custom")
        writer.writerow(g)
        writer.writerow(a)
        writer.writerow(r)

calibrate(10)
    
    
    


